"use strict";

const Alpaca = require("@alpacahq/alpaca-trade-api");

class DataStream {
  constructor({ feed }) {
    this.alpaca = new Alpaca({
      keyId: process.env.ALPACA_API_KEY,
      secretKey: process.env.ALPACA_SECRET_KEY,
      paper: process.env.ALPACA_PAPER,
      feed: feed,
    });

    const socket = this.alpaca.data_stream_v2;

    socket.onConnect(function () {
      //socket.subscribeForQuotes(["AAPL"]);
      //socket.subscribeForTrades(["FB"]);
      socket.subscribeForBars(["SPY"]);
      //socket.subscribeForStatuses(["*"]);
    });

    socket.onError((err) => {
      console.log("Error:", err);
    });

    // socket.onStockTrade((trade) => {
    //   console.log(trade);
    // });

    // socket.onStockQuote((quote) => {
    //   console.log(quote);
    // });

    socket.onStockBar((bar) => {
      console.log(bar);
    });

    // socket.onStatuses((s) => {
    //   console.log(s);
    // });

    socket.onStateChange((state) => {
      console.log(state);
    });

    socket.onDisconnect(() => {
      console.log("Disconnected");
    });

    socket.connect();

  }
}

let stream = new DataStream({
  feed: "iex"
});