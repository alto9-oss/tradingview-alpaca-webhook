const TradingViewController = require('./controllers/tradingview.controller');

exports.routesConfig = function (app) {
    app.post('/tradingview/:key/:type/', [
        TradingViewController.handleTrade
    ])
}