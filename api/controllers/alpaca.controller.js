const Alpaca = require('@alpacahq/alpaca-trade-api')

module.exports = class AlpacaController {
    constructor() {
        this.alpaca = new Alpaca({
            keyId: process.env.ALPACA_API_KEY,
            secretKey: process.env.ALPACA_SECRET_KEY,
            paper: process.env.ALPACA_PAPER
        })
    }

    getAlpaca = () => {
        return this.alpaca
    }

    getAccount = async () => {
        return await this.alpaca.getAccount()
    }

    closePosition = async (ticker) => {

    }

    executeBracketOrder = (ticker, contracts, limitPrice, stopPrice) => {

        let order = {
            symbol: ticker,
            qty: contracts,
            side: 'buy',
            type: 'market',
            time_in_force: 'day',
            order_class: 'bracket',
            take_profit: {
                limit_price: limitPrice
            },
            stop_loss: {
                stop_price: stopPrice
            }
        }

        //return await this.alpaca.createOrder(order)
        return order
    }

}